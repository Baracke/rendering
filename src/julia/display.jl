module Display

using GLFW
using ModernGL

export BitMap, drawPixel, createWindow

struct BitMap
    width::Int64
    height::Int64
    data::Array{GLfloat,1}
end

BitMap(width, height) =
    BitMap(width, height, zeros(GLfloat, width * height * 3))

function drawPixel(
    screen::BitMap,
    x::Int64,
    y::Int64,
    r::GLfloat,
    g::GLfloat,
    b::GLfloat,
    #a::GLfloat
)
    index = (x + screen.width * y) * 3 + 1
    @inbounds screen.data[index] = r
    @inbounds screen.data[index+1] = g
    @inbounds screen.data[index+2] = b
    #@inbounds screen.data[index+3] = a
end

function drawPixel(
    screen::BitMap,
    x::Int64,
    y::Int64,
    r::Float64,
    g::Float64,
    b::Float64,
    #a::Float64
)
    drawPixel(screen, x, y, GLfloat(r), GLfloat(g), GLfloat(b))#, GLfloat(a))
end

function glDrawPixels(width, height, format, type, data)
    ccall(
        @eval(GLFW.GetProcAddress("glDrawPixels")),
        Cvoid,
        (GLsizei, GLsizei, GLenum, GLenum, Ptr{Cvoid}),
        width,
        height,
        format,
        type,
        data,
    )
end

function createWindow(width, height, title, renderBitMap)
    screen = BitMap(width, height)
    # Create a window and its OpenGL context
    window = GLFW.CreateWindow(screen.width, screen.height, title)

    # Make the window's context current
    GLFW.MakeContextCurrent(window)

    lastTime = time_ns()
    fps = 0
    fpsTime = 0
    # Loop until the user closes the window
    while !GLFW.WindowShouldClose(window)
        delta = time_ns() - lastTime
        lastTime += delta
        renderBitMap(screen, delta / 1000000000)
        # draw screen data to framebuffer
        glDrawPixels(
            screen.width,
            screen.height,
            GL_RGB,
            GL_FLOAT,
            screen.data
        )
        # Swap front and back buffers
        GLFW.SwapBuffers(window)

        # Poll for and process events
        GLFW.PollEvents()
        fps += 1
        fpsTime += delta
        if (fpsTime > 1000000000)
            println("fps: $fps")
            fps = 0
            fpsTime = 0
        end
    end

    GLFW.DestroyWindow(window)
end

end
