function grandient(screen, delta)
    fill!(screen.data, 0)
    for y::Int64 = 0:screen.height-1
        for x::Int64 = 0:screen.width-1
            Display.drawPixel(screen, x, y, x / screen.width, y / screen.height, 0.0) #, 1.0)
        end
    end
end

 Display.createWindow(640, 480, "Grandient Example", grandient)
