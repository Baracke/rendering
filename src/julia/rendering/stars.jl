mutable struct Star
    x::Float64
    y::Float64
    z::Float64
end

function initStar(star::Star)
    star.x = 2 * (rand() - 0.5)
    star.y = 2 * (rand() - 0.5)
    star.z = rand() + 0.0001
    star
end

function updateAndRenderStars(screen, delta, stars, speed)
    fill!(screen.data, 0)
    tanHalfFoV = tand(160/2) # Field of view of 160 degrees, so the 'star box' is visible
    halfWidth  = screen.width/2
    halfHeight = screen.height/2

    for star in stars
        star.z -= speed * delta
        x = star.x / (star.z * tanHalfFoV) * halfWidth + halfWidth
        y = star.y / (star.z * tanHalfFoV) * halfHeight + halfHeight

        if (star.z > 0 && x >= 0 && x <= screen.width && y >= 0 && y <= screen.height)
            Display.drawPixel(screen, trunc(Int64, x), trunc(Int64, y), 1.0, 1.0, 1.0)
        else
            initStar(star)
        end
    end
end


Display.createWindow(640, 480, "Stars",
     let stars = map(star -> initStar(Star(0,0,0)), fill(Star(0,0,0), 3000))
         function (screen, delta)
             updateAndRenderStars(screen, delta, stars, 0.06)
         end
     end)
